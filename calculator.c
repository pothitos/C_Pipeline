#include "operations.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
        int first_operand;
        int second_operand;
        char operation;
        if (argc == 4) {
                first_operand = atoi(argv[1]);
                operation = argv[2][0];
                second_operand = atoi(argv[3]);
                if (operation == '+') {
                        printf("%d\n", add(first_operand, second_operand));
                } else if (operation == '-') {
                        printf("%d\n", subtract(first_operand, second_operand));
                } else if (operation == '*') {
                        printf("%d\n", multiply(first_operand, second_operand));
                } else {
                        fprintf(stderr, "Unsupported operation!\n");
                        return 1;
                }
        } else {
                fprintf(stderr, "Usage: ./calculator <x> <operation> <y>\n");
                return 1;
        }
        return 0;
}
