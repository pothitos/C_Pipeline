#include "../operations.h"
#include "minunit.h"
#include <stdio.h>

static int tests_run = 0;

static char* unit_test_addition()
{
        mu_assert(add(4, 3) == 7, "Wrong add() result");
        return 0;
}

static char* unit_test_subtraction()
{
        mu_assert(subtract(7, 2) == 5, "Wrong subtract() result");
        return 0;
}

static char* unit_test_multiplication()
{
        mu_assert(multiply(2, 8) == 16, "Wrong multiply() result");
        return 0;
}

static char* all_tests()
{
        mu_run_test(unit_test_addition);
        mu_run_test(unit_test_subtraction);
        mu_run_test(unit_test_multiplication);
        return 0;
}

int main()
{
        char* result = all_tests();
        int test_failed = (result != 0);
        if (test_failed)
                printf("%s\n", result);
        else
                printf("ALL TESTS PASSED\n");
        printf("Tests run: %d\n", tests_run);
        return test_failed;
}
